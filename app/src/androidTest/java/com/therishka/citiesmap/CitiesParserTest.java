package com.therishka.citiesmap;

import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.therishka.citiesmap.models.City;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.InputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Rishad Mustafaev
 */
@RunWith(AndroidJUnit4.class)
public class CitiesParserTest {

    private static final int EXACT_CITIES_NUMBER = 209557;

    private CitiesParser parser;

    private long startTestTimeInMillis;

    @Before
    public void init() {
        parser = new CitiesParser();
        startTimeTracking();
    }

    @After
    public void trackParsingTime() {
        stopTimeTrackingAndLog();
    }

    @Test
    public void testCitiesParsing() {
        InputStream jsonInputStream = createTestInputStream();
        startTimeTracking();
        List<City> parsedFullCities =
                parser.parseFullCities(jsonInputStream);
        stopTimeTrackingAndLog();
        Assert.assertTrue(parsedFullCities.size() == EXACT_CITIES_NUMBER);
        Assert.assertTrue(parsedFullCities.get(0).getCoordinates() != null);
    }

    private void startTimeTracking() {
        logMessage("Start time tracking");
        startTestTimeInMillis = System.currentTimeMillis();
    }

    /**
     * Not sure how to output to test outputs, so check logcat.
     */
    private void stopTimeTrackingAndLog() {
        long timeStopped = System.currentTimeMillis();
        long timeInSeconds = TimeUnit.MILLISECONDS
                .toSeconds(timeStopped - startTestTimeInMillis);
        logMessage("Result time = "
                + timeInSeconds
                + " time in seconds");
        logMessage("Time tracking stopped");
    }

    private void logMessage(@NonNull String message) {
        System.out.println("CitiesParserTest: " + message);
    }

    @NonNull
    private InputStream createTestInputStream() {
        return InstrumentationRegistry
                .getTargetContext()
                .getResources()
                .openRawResource(R.raw.cities);
    }
}
