package com.therishka.citiesmap;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.therishka.citiesmap.models.City;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Rishad Mustafaev
 * Helper class that encapsulates parsing of big json.
 * It uses Reader style, to reduce memory consumption, while
 * parsing with Gson.
 */
public final class CitiesParser {

    @NonNull
    private final Type fullCityType;

    @NonNull
    private final Gson gson;

    public CitiesParser() {
        fullCityType = new TypeToken<List<City>>() {
        }.getType();
        gson = new Gson();
    }

    /**
     * Parses big raw input into POJO's I need.
     * Do not use over MainThread, it takes like 9 seconds on weak devices to parse it.
     *
     * @param jsonFileStream inputStream of json raw resource.
     * @return List of parsed items. In this case - list of City objects.
     */
    @WorkerThread
    @NonNull
    public List<City> parseFullCities(@NonNull InputStream jsonFileStream) {
        List<City> result = new ArrayList<>();

        InputStreamReader rawResourceReader = getRawStreamReader(jsonFileStream);
        if (rawResourceReader != null) {
            JsonReader reader = new JsonReader(rawResourceReader);
            result = gson.fromJson(reader, fullCityType);
            Collections.sort(result);
        }
        return result;
    }

    @Nullable
    private InputStreamReader getRawStreamReader(@NonNull InputStream jsonFileStream) {
        InputStreamReader rawResourceReader = null;
        try {
            rawResourceReader =
                    new InputStreamReader(jsonFileStream, "UTF-8");
        } catch (UnsupportedEncodingException exception) {
            logMessage(exception.getMessage());
        }
        return rawResourceReader;
    }

    private void logMessage(@NonNull String message) {
        Log.w(this.getClass().getSimpleName(), message);
    }
}
