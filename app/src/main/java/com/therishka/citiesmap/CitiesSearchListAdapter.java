package com.therishka.citiesmap;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.therishka.citiesmap.models.City;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Rishad Mustafaev
 */
public class CitiesSearchListAdapter extends
        RecyclerView.Adapter<CitiesSearchListAdapter.CityViewHolder> {

    @NonNull
    private final List<City> sortedItems = new ArrayList<>();

    @Nullable
    private List<City> allItems;

    CitiesSearchListAdapter() {
//        sortedItems = new SortedList<>(City.class,
//                new SearchCitiesAdapterCallback(this));
    }

    @NonNull
    @Override
    public CitiesSearchListAdapter.CityViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                     int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cities_list_item, parent, false);
        return new CityViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CitiesSearchListAdapter.CityViewHolder holder,
                                 int position) {
        City city = sortedItems.get(position);
        holder.bind(city.getName(), city.getCountry());
    }

    @Override
    public int getItemCount() {
        return sortedItems.size();
    }

    public void setInitialData(@NonNull List<City> cities) {
        allItems = cities;
        showAllItems();
    }

    private void showAllItems() {
        if (allItems == null) {
            return;
        }
        if (sortedItems.size() > 0) {
            sortedItems.clear();
        }
//        sortedItems.beginBatchedUpdates();
        sortedItems.addAll(allItems);
//        sortedItems.endBatchedUpdates();
    }

    private void performUpdate(@NonNull List<City> filteredCities) {
//        sortedItems.beginBatchedUpdates();
        sortedItems.clear();
        sortedItems.addAll(filteredCities);
        notifyDataSetChanged();
//        sortedItems.endBatchedUpdates();
    }

    public void performFilter(@Nullable String query) {
        if (query == null || "".equals(query)) {
            // if query is empty - display all items.
            showAllItems();
            return;
        }
        if (allItems != null) {
            List<City> filteredCities = new ArrayList<>();
            // fake city just for easier compareTo usage
            City fakeCity = new City(query, query, 0, null);
            int indexOfFirstFoundElement = Collections.binarySearch(allItems,
                    fakeCity);
            if (indexOfFirstFoundElement < 0 || indexOfFirstFoundElement > allItems.size()) {
                // no elements found
                performUpdate(filteredCities);
                return;
            }
            // add the first found element
            filteredCities.add(allItems.get(indexOfFirstFoundElement));
            for (int i = indexOfFirstFoundElement; i < allItems.size(); i++) {
                City city = allItems.get(i);
                if (city.compareTo(fakeCity) >= 0) {
                    filteredCities.add(city);
                } else {
                    break;
                }
            }
            performUpdate(filteredCities);
        }
    }

    static class CityViewHolder extends RecyclerView.ViewHolder {

        @NonNull
        TextView cityNameTextView;

        CityViewHolder(View itemView) {
            super(itemView);
            cityNameTextView =
                    itemView.findViewById(R.id.cities_list_item_city_name);
        }

        void bind(@NonNull String cityName,
                  @NonNull String cityCountry) {
            String fullCityName = itemView.getResources()
                    .getString(R.string.cities_list_city_item_text,
                            cityName, cityCountry);
            cityNameTextView.setText(fullCityName);
        }


    }
}
