package com.therishka.citiesmap;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * @author Rishad Mustafaev
 */
public class SimpleSearchView extends LinearLayout {

    @Nullable
    private EditText searchEditText;

    @Nullable
    private View searchIcon;

    @Nullable
    private View clearInputButton;

    @Nullable
    private OnSearchQueryChangedListener onSearchQueryChangedListener;

    @NonNull
    private final TextWatcher searchTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // do nothing...
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // do nothing...
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s == null) {
                return;
            }
            String query = s.toString();
            if (clearInputButton == null || searchIcon == null) {
                return;
            }
            if (onSearchQueryChangedListener != null) {
                onSearchQueryChangedListener.onSearchQueryChanged(query);
            }

            if ((!query.isEmpty()) && clearInputButton.getVisibility() == VISIBLE) {
                setClearInputIconVisibility(false);
            }
            if (!query.isEmpty() && clearInputButton.getVisibility() != VISIBLE) {
                setClearInputIconVisibility(true);
            }
        }
    };

    public SimpleSearchView(Context context) {
        super(context);
        init(context);
    }

    public SimpleSearchView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SimpleSearchView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(@Nullable Context context) {
        if (context == null) {
            return;
        }
        inflate(context, R.layout.simple_search_view_layout, this);
        setDescendantFocusability(FOCUS_BEFORE_DESCENDANTS);
        setFocusableInTouchMode(true);
        setOrientation(HORIZONTAL);

        searchEditText = findViewById(R.id.simple_search_view_text_input);
        clearInputButton = findViewById(R.id.simple_search_view_clear_icon);
        searchIcon = findViewById(R.id.simple_search_view_search_icon);

        if (searchEditText != null && clearInputButton != null) {
            searchEditText.addTextChangedListener(searchTextWatcher);
            clearInputButton.setOnClickListener(v -> clearInputClicked());
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if (searchEditText != null) {
            searchEditText.removeTextChangedListener(searchTextWatcher);
        }
        super.onDetachedFromWindow();
    }

    private void setClearInputIconVisibility(boolean isVisible) {
        if (clearInputButton != null) {
            setViewVisibility(isVisible, clearInputButton);
        }
    }

    private void setViewVisibility(boolean isVisible, @NonNull View view) {
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
            view.animate()
                    .alpha(1)
                    .setListener(null)
                    .start();
        } else {
            view.animate()
                    .alpha(0)
                    .withEndAction(() -> view.setVisibility(INVISIBLE))
                    .start();
        }
    }

    private void clearInputClicked() {
        if (searchEditText != null) {
            searchEditText.setText("");
        }
    }

    private void setTextSilently(@Nullable String text) {
        if (searchEditText != null) {
            searchEditText.removeTextChangedListener(searchTextWatcher);
            searchEditText.setText(text == null ? "" : text);
            searchEditText.addTextChangedListener(searchTextWatcher);
        }
    }

    public void restoreText(@Nullable String text) {
        if (searchEditText != null) {
            setTextSilently(text);
            // after restoration, user selection should be at the end
            searchEditText.setSelection(searchEditText.getText().length());
        }
    }

    @Nullable
    public String getCurrentText() {
        if (searchEditText != null) {
            return searchEditText.getText().toString();
        }
        return null;
    }

    public void setOnSearchQueryChangedListener(@Nullable OnSearchQueryChangedListener onSearchQueryChangedListener) {
        this.onSearchQueryChangedListener = onSearchQueryChangedListener;
    }

    interface OnSearchQueryChangedListener {
        void onSearchQueryChanged(@NonNull String searchQuery);
    }
}
