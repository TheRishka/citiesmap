package com.therishka.citiesmap;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

import com.therishka.citiesmap.models.City;

import java.util.List;

/**
 * @author Rishad Mustafaev
 */
public class MainActivity extends AppCompatActivity implements
        CitiesListFragment.CitiesListInteractionListener,
        CitiesListFragment.FragmentReadyListener,
        LoaderManager.LoaderCallbacks<List<City>> {

    private static final int CITIES_FETCH_LOADER_ID = 111;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_fragment_container,
                        new CitiesListFragment(),
                        CitiesListFragment.TAG)
                .commit();
    }

    @Override
    public void onItemClicked(long cityId) {

    }

    @Override
    public void onFragmentReady() {
        getSupportLoaderManager().initLoader(CITIES_FETCH_LOADER_ID,
                null, this);
    }

    private void notifyFragmentIfPossible(@NonNull List<City> cities) {
        Fragment fragment =
                getSupportFragmentManager().findFragmentByTag(CitiesListFragment.TAG);
        if (fragment != null && fragment.getUserVisibleHint()) {
            ((CitiesListFragment) fragment).citiesAvailable(cities);
        }
    }

    @NonNull
    @Override
    public Loader<List<City>> onCreateLoader(int id, @Nullable Bundle args) {
        return new CitiesFetchLoader(this);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<City>> loader, List<City> cities) {
        notifyFragmentIfPossible(cities);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<City>> loader) {

    }
}
