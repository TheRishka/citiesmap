package com.therishka.citiesmap;

import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;

import com.therishka.citiesmap.models.City;

/**
 * @author Rishad Mustafaev
 */
public class SearchCitiesAdapterCallback extends SortedList.Callback<City> {

    @NonNull
    private final CitiesSearchListAdapter adapter;

    public SearchCitiesAdapterCallback(@NonNull CitiesSearchListAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public int compare(City cityOne, City cityTwo) {
        return cityOne.getName().compareTo(cityTwo.getName());
    }

    @Override
    public void onChanged(int position, int count) {
        adapter.notifyItemRangeChanged(position, count);
    }

    @Override
    public boolean areContentsTheSame(City oldItem, City newItem) {
        return isTheSame(oldItem, newItem);
    }

    @Override
    public boolean areItemsTheSame(City itemOne, City itemTwo) {
        return isTheSame(itemOne, itemTwo);
    }

    private boolean isTheSame(City oldItem, City newItem) {
        return oldItem.getId() == newItem.getId();
    }

    @Override
    public void onInserted(int position, int count) {
        adapter.notifyItemRangeInserted(position, count);
    }

    @Override
    public void onRemoved(int position, int count) {
        adapter.notifyItemRangeRemoved(position, count);
    }

    @Override
    public void onMoved(int fromPosition, int toPosition) {
        adapter.notifyItemMoved(fromPosition, toPosition);
    }
}
