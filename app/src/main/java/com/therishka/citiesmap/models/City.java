package com.therishka.citiesmap.models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Rishad Mustafaev
 * Simple POJO for Gson parsing.
 */
public class City implements Comparable<City> {

    @NonNull
    private final String country;

    @NonNull
    private final String name;

    @SerializedName("_id")
    private
    long id;

    @SerializedName("coord")
    @Nullable
    private final Coordinates coordinates;

    public City(@NonNull String country,
                @NonNull String name,
                long id,
                @Nullable Coordinates coordinates) {
        this.country = country;
        this.name = name;
        this.id = id;
        this.coordinates = coordinates;
    }

    @NonNull
    public String getCountry() {
        return country;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    @Nullable
    public Coordinates getCoordinates() {
        return coordinates;
    }

    @Override
    public int compareTo(@NonNull City o) {
        return getName().toLowerCase()
                .compareTo(o.getName().toLowerCase());
    }
}
