package com.therishka.citiesmap;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.therishka.citiesmap.models.City;

import java.util.List;

public class CitiesListFragment extends Fragment implements
        CitiesFetchObserver {

    public static final String TAG = CitiesListFragment.class.getSimpleName();

    @Nullable
    private CitiesListInteractionListener citiesListInteractionListener;

    @Nullable
    private FragmentReadyListener fragmentReadyListener;

    @Nullable
    private RecyclerView citiesList;

    @Nullable
    private CitiesSearchListAdapter citiesListAdapter;

    @Nullable
    private SimpleSearchView simpleSearchView;

    @Nullable
    private View progressView;

    public CitiesListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cities_list, container, false);
        initList(view);
        initSearch(view);
        progressView = view.findViewById(R.id.cities_list_progress);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showLoadingAndHideList();
        if (fragmentReadyListener != null) {
            fragmentReadyListener.onFragmentReady();
        }
    }

    private void initSearch(@NonNull View rootView) {
        simpleSearchView = rootView.findViewById(R.id.cities_list_search);
        if (simpleSearchView != null) {
            simpleSearchView.setOnSearchQueryChangedListener(query -> {
                if (citiesListAdapter != null && citiesList != null) {
                    citiesListAdapter.performFilter(query);
                    citiesList.scrollToPosition(0);
                }
            });
        }
    }

    private void initList(@NonNull View rootView) {
        citiesList = rootView.findViewById(R.id.cities_list);
        if (citiesList != null) {
            citiesList.setLayoutManager(new LinearLayoutManager(getActivity()));
            citiesListAdapter = new CitiesSearchListAdapter();
            citiesList.setAdapter(citiesListAdapter);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CitiesListInteractionListener &&
                context instanceof FragmentReadyListener) {
            citiesListInteractionListener = (CitiesListInteractionListener) context;
            fragmentReadyListener = ((FragmentReadyListener) context);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement CitiesListInteractionListener"
                    + " and FragmentReadyListener!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        citiesListInteractionListener = null;
        fragmentReadyListener = null;
    }

    @Override
    public void citiesAvailable(@NonNull List<City> cities) {
        if (citiesListAdapter != null && citiesList != null) {
            citiesListAdapter.setInitialData(cities);
        }
        hideLoadingAndShowList();
    }

    private void showLoadingAndHideList() {
        if (progressView != null) {
            progressView.setVisibility(View.VISIBLE);
        }
        if (citiesList != null) {
            citiesList.setVisibility(View.GONE);
        }
        if (simpleSearchView != null) {
            simpleSearchView.setVisibility(View.GONE);
        }
    }

    private void hideLoadingAndShowList() {
        if (progressView != null) {
            progressView.setVisibility(View.GONE);
        }
        if (citiesList != null) {
            citiesList.setVisibility(View.VISIBLE);
        }
        if (simpleSearchView != null) {
            simpleSearchView.setVisibility(View.VISIBLE);
        }
    }

    public interface CitiesListInteractionListener {
        // TODO: Update argument type and name
        void onItemClicked(long cityId);
    }

    public interface FragmentReadyListener {
        void onFragmentReady();
    }
}
