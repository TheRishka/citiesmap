package com.therishka.citiesmap;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import com.therishka.citiesmap.models.City;

import java.io.InputStream;
import java.util.List;

/**
 * @author Rishad Mustafaev
 * 3rd-party libraries are restricted. so I wasn't sure if I was allowed to use
 * Architecture Components, so I decided to use Loaders as a way to
 * keep data that survives rotation.
 * <p>
 * AsyncTasks are definately worse, so Loaders are the only choice.
 */
public class CitiesFetchLoader extends AsyncTaskLoader<List<City>> {

    @Nullable
    private List<City> cities;

    public CitiesFetchLoader(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        if (cities != null && cities.size() > 0) {
            deliverResult(cities);
        } else {
            forceLoad();
        }
    }

    @Nullable
    @Override
    public List<City> loadInBackground() {
        InputStream jsonFileInputStream = getContext()
                .getResources()
                .openRawResource(R.raw.cities);
        return new CitiesParser().parseFullCities(jsonFileInputStream);
    }

    @Override
    public void deliverResult(@Nullable List<City> data) {
        cities = data;
        super.deliverResult(data);
    }
}
