package com.therishka.citiesmap;

import android.support.annotation.NonNull;

import com.therishka.citiesmap.models.City;

import java.util.List;

/**
 * @author Rishad Mustafaev
 */
public interface CitiesFetchObserver {

    void citiesAvailable(@NonNull List<City> cities);
}
